package handler

import (
	"fmt"
	"log/slog"
	"mime/multipart"
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	MaxSizeFile = 10 << 20 // 10 Mb
)

type ReqForm struct {
	File multipart.File `json:"file"`
	Key  string         `json:"key"`
}

type Service interface {
	EncodeSymmetric(file multipart.File, key string) ([]byte, error)
	DecodeSymmetric(file multipart.File, key string) ([]byte, error)
}

type Handler struct {
	logger  *slog.Logger
	service Service
}

func New(service Service, logger *slog.Logger) *Handler {
	return &Handler{
		logger:  logger,
		service: service,
	}
}

func Init(api *gin.RouterGroup, service Service, logger *slog.Logger) {
	h := New(service, logger)

	api.POST("/decode/symmetric", h.DecodeSymmetric)
	api.POST("/encode/symmetric", h.EncodeSymmetric)
}

func (h *Handler) DecodeSymmetric(c *gin.Context) {
	var form ReqForm
	if err := c.ShouldBind(&form); err != nil {
		h.logger.Error(err.Error(), "cant parse request body")
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "cant parse request body",
		})
	}

	if err := c.Request.ParseMultipartForm(MaxSizeFile); err != nil {
		h.logger.Error(err.Error(), "file must be less than 10 Mb")
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "file must be less than 10 Mb",
		})
		return
	}

	file, fileHeader, err := c.Request.FormFile("file")
	if err != nil {
		h.logger.Error(err.Error(), "cannot get file from request")
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	bytesDecode, err := h.service.DecodeSymmetric(file, form.Key)

	fileContentType := fileHeader.Header.Get("Content-type")

	c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s", fileHeader.Filename))
	c.Header("Content-Type", fileContentType)
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Length", fmt.Sprintf("%d", len(bytesDecode)))

	c.Data(http.StatusOK, "application/octet-stream", bytesDecode)
}

func (h *Handler) EncodeSymmetric(c *gin.Context) {
	var form ReqForm
	if err := c.ShouldBind(&form); err != nil {
		h.logger.Error(err.Error(), "cant parse request body")
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "cant parse request body",
		})
	}

	if err := c.Request.ParseMultipartForm(MaxSizeFile); err != nil {
		h.logger.Error(err.Error(), "file must be less than 10 Mb")
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "file must be less than 10 Mb",
		})
		return
	}

	file, fileHeader, err := c.Request.FormFile("file")
	if err != nil {
		h.logger.Error(err.Error(), "cannot get file from request")
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	bytesEncoded, err := h.service.EncodeSymmetric(file, form.Key)

	fileContentType := fileHeader.Header.Get("Content-type")

	c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s", fileHeader.Filename))
	c.Header("Content-Type", fileContentType)
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Length", fmt.Sprintf("%d", len(bytesEncoded)))

	c.Data(http.StatusOK, "application/octet-stream", bytesEncoded)
}
