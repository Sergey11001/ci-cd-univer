package service

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSymmetricCode(t *testing.T) {
	text := "test"

	encodeBytes, err := encodeSymmetric([]byte(text), "test")
	assert.Nil(t, err)

	decodeBytes, err := decodeSymmetric(encodeBytes, "test")
	assert.Nil(t, err)

	assert.Equal(t, text, string(decodeBytes))
}
