package service

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"io"
	"log/slog"
	"mime/multipart"

	"golang.org/x/crypto/pbkdf2"
)

const (
	SaltLength = 12
)

type Serv struct {
	logger *slog.Logger
}

func New(logger *slog.Logger) *Serv {
	return &Serv{logger: logger}
}

func (s *Serv) EncodeSymmetric(file multipart.File, key string) ([]byte, error) {
	plainText, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return encodeSymmetric(plainText, key)
}

func (s *Serv) DecodeSymmetric(file multipart.File, key string) ([]byte, error) {
	cipherText, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return decodeSymmetric(cipherText, key)
}

func encodeSymmetric(data []byte, key string) ([]byte, error) {
	nonce := make([]byte, SaltLength)
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}

	byteKey := []byte(key)
	dk := pbkdf2.Key(byteKey, nonce, 4096, 32, sha1.New)

	block, err := aes.NewCipher(dk)
	if err != nil {
		return nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	cipherText := aesgcm.Seal(nil, nonce, data, nil)
	cipherText = append(cipherText, nonce...)

	return cipherText, nil
}

func decodeSymmetric(data []byte, key string) ([]byte, error) {
	salt := data[len(data)-SaltLength:]
	str := hex.EncodeToString(salt)
	nonce, err := hex.DecodeString(str)

	dk := pbkdf2.Key([]byte(key), nonce, 4096, 32, sha1.New)

	block, err := aes.NewCipher(dk)

	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	plainText, err := aesgcm.Open(nil, nonce, data[:len(data)-SaltLength], nil)
	if err != nil {
		return nil, err
	}

	return plainText, nil
}
