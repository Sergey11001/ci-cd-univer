package main

import (
	"ci-cd-univer/pkg/handler"
	"ci-cd-univer/pkg/service"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"log/slog"
)

func main() {
	r := gin.Default()
	r.Use(cors.Default())

	api := r.Group("/api")

	logger := &slog.Logger{}
	serv := service.New(logger)
	handler.Init(api, serv, logger)

	if err := r.Run(":8000"); err != nil {
		logger.Error("could not run server")
	}
}
